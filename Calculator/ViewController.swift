//
//  ViewController.swift
//  Calculator
//
//  Created by Mubeen Nisar on 6/13/16.
//  Copyright © 2016 Sleek Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var display: UILabel!
    private var isTyping = false
    @IBAction private func touchDigit(sender: UIButton) {
        if let title = sender.currentTitle {
            if isTyping {
                let currentText=display.text
                display.text = currentText! + title
            }else{
                display.text = title
            }
            isTyping = true
        }
        
        
    }
    private var displayValue : Double{
        get{
            return Double(display.text!)!
        }set{
            display.text = String(newValue)
        }
    }
    
    private var brain = CalculatorBrain()
    
    @IBAction private func performOperation(sender: UIButton) {
        
        if isTyping {
            brain.setOperand(displayValue)
            isTyping = false
        }
        if let symbol = sender.currentTitle {
            
            brain.performOperation(symbol)
            
        }
        displayValue = brain.result
    }
    
    var savedProgram: CalculatorBrain.PropertyList?
    
    @IBAction func saveProgram() {
        savedProgram = brain.program
    }
    
    
    @IBAction func restoreProgram() {
        if savedProgram != nil {
            brain.program = savedProgram!
            displayValue = brain.result
        }
    }
}

